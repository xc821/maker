package ldh.maker.freemaker;

import ldh.database.Table;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by ldh on 2017/4/16.
 */
public class JavafxSearchControllerMaker extends BeanMaker<JavafxSearchControllerMaker> {

    protected Table table;
    protected String projectPackage;
    protected String pojoPackage;
    protected String enumProjectPackage;
    protected String author;

    public JavafxSearchControllerMaker table(Table table) {
        this.table = table;
        return this;
    }

    public JavafxSearchControllerMaker projectPackage(String projectPackage) {
        this.projectPackage = projectPackage;
        return this;
    }

    public JavafxSearchControllerMaker pojoPackage(String pojoPackage) {
        this.pojoPackage = pojoPackage;
        return this;
    }

    public JavafxSearchControllerMaker enumProjectPackage(String enumProjectPackage) {
        this.enumProjectPackage = enumProjectPackage;
        return this;
    }

    public JavafxSearchControllerMaker author(String author) {
        this.author = author;
        return this;
    }

    @Override
    public JavafxSearchControllerMaker make() {
        data();
        out(ftl, data);
        return this;
    }

    @Override
    public void data() {
        fileName = table.getJavaName() + "SearchController.java";
        data.put("table", table);
        data.put("projectPackage", projectPackage);
        data.put("pojoPackage", pojoPackage);
        data.put("controllerPackage", pack);
        data.put("enumProjectPackage", enumProjectPackage);
        SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String str=sdf.format(new Date());
        data.put("Author", author);
        data.put("DATE", str);
        data.put("Description",description);

    }
}
