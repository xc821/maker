package ${projectPackage};

import com.jfoenix.controls.JFXDecorator;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.Region;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;
import ${projectPackage}.page.LoginPage;
import ldh.fx.transition.FadeInRightBigTransition;
import ldh.fx.transition.FadeOutUpBigTransition;
import ldh.fx.StageUtil;

public class DeskClientMain extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {
        StageUtil.STAGE = primaryStage;
        Parent root = FXMLLoader.load(getClass().getResource("/fxml/Home.fxml"));
        JFXDecorator jfxDecorator = new JFXDecorator(primaryStage, root);
        jfxDecorator.setOnCloseButtonAction(()->{
            FadeOutUpBigTransition fadeOutUpBigTransition = new FadeOutUpBigTransition(jfxDecorator);
            fadeOutUpBigTransition.setOnFinished(e->primaryStage.close());
            fadeOutUpBigTransition.playFromStart();
        });

        Scene scene = new Scene(jfxDecorator, 1200, 600);
        scene.getStylesheets().add("/css/common.css");
        scene.getStylesheets().add("/component/LNavPane.css");
        scene.getStylesheets().add("/component/LxDialog.css");
        scene.getStylesheets().add("/component/GridTable.css");
        scene.getStylesheets().add("/css/Form.css");
        primaryStage.setTitle("智能代码生成器之生成javafx desktop client代码");

        primaryStage.initStyle(StageStyle.TRANSPARENT);
        jfxDecorator.setOpacity(0);
        scene.setFill(null);
        primaryStage.setScene(scene);

        showLogin(primaryStage, jfxDecorator);
    }

    private void showLogin(Stage primaryStage, Node node) {
        Stage stage = new Stage();
        stage.initOwner(primaryStage);
        LoginPage loginPage = new LoginPage(450, 420);
        loginPage.setConsumer((Void)->{
            primaryStage.show();
            FadeInRightBigTransition fadeInRightTransition = new FadeInRightBigTransition(node);
            fadeInRightTransition.playFromStart();
        });
        loginPage.setStage(stage);
        Scene scene = new Scene(loginPage);
        stage.setScene(scene);
        scene.setFill(null);
        stage.initStyle(StageStyle.TRANSPARENT);
        stage.centerOnScreen();

        loginPage.setOpacity(0);
        stage.show();

        FadeInRightBigTransition fadeInRightTransition = new FadeInRightBigTransition(loginPage);
        fadeInRightTransition.setDelay(new Duration(500));
        fadeInRightTransition.playFromStart();
    }
}