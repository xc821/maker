package ${projectPackage};

/**
 * @author: ${Author}
 * @date: ${DATE}
 */

import io.vertx.core.*;
import ${projectPackage}.verticle.MainHttpRouter;
import ${projectPackage}.verticle.router.*;

import java.util.concurrent.CountDownLatch;

public class VertxApplication {

    public static final void main(String[] args) throws InterruptedException {
        System.setProperty("vertx.logger-delegate-factory-class-name", "io.vertx.core.logging.SLF4JLogDelegateFactory");
//        InternalLoggerFactory.setDefaultFactory(Log4JLoggerFactory.INSTANCE);
        VertxOptions vertxOptions = new VertxOptions();
        vertxOptions.setEventLoopPoolSize(4);
        Vertx vertx = Vertx.vertx(vertxOptions);

        DeploymentOptions deploymentOptions = new DeploymentOptions();
        deploymentOptions.setWorkerPoolSize(500);
        deploymentOptions.setInstances(Runtime.getRuntime().availableProcessors() * 2);

        CountDownLatch mainVerticle = new CountDownLatch(1);
        vertx.deployVerticle(MainHttpRouter.class.getName(), ra->{
            if (ra.succeeded()) {
                mainVerticle.countDown();
            } else {
                throw new RuntimeException("error!!!");
            }
        });
        mainVerticle.await();

        <#list tableInfo.tables?keys as key>
            <#if !tableInfo.tables[key].middle && tableInfo.tables[key].create && tableInfo.tables[key].primaryKey?? && !tableInfo.tables[key].primaryKey.composite>
        deployVerticle(vertx, ${tableInfo.tables[key].javaName}Router.class, deploymentOptions);
            </#if>
        </#list>
    }

    private static void deployVerticle(Vertx vertx, Class clazz, DeploymentOptions deploymentOptions) {
        vertx.deployVerticle(clazz, deploymentOptions, ra1->{
            if (!ra1.succeeded()) {
                ra1.cause().printStackTrace();
            }
        });
    }
}
