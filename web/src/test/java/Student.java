import javafx.beans.property.*;
import javafx.collections.ObservableList;

/**
 * Created by ldh123 on 2018/5/29.
 */
public class Student {

    private LongProperty idProperty = new SimpleLongProperty();
    private StringProperty nameProperty = new SimpleStringProperty();
    private DoubleProperty score = new SimpleDoubleProperty();
    private ListProperty<Student> students = new SimpleListProperty<>();

    public long getIdProperty() {
        return idProperty.get();
    }

    public LongProperty idPropertyProperty() {
        return idProperty;
    }

    public void setIdProperty(long idProperty) {
        this.idProperty.set(idProperty);
    }

    public String getNameProperty() {
        return nameProperty.get();
    }

    public StringProperty namePropertyProperty() {
        return nameProperty;
    }

    public void setNameProperty(String nameProperty) {
        this.nameProperty.set(nameProperty);
    }

    public double getScore() {
        return score.get();
    }

    public DoubleProperty scoreProperty() {
        return score;
    }

    public void setScore(double score) {
        this.score.set(score);
    }

    public ObservableList<Student> getStudents() {
        return students.get();
    }

    public ListProperty<Student> studentsProperty() {
        return students;
    }

    public void setStudents(ObservableList<Student> students) {
        this.students.set(students);
    }
}
