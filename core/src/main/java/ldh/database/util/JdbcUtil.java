package ldh.database.util;

import java.util.HashMap;
import java.util.Map;

public class JdbcUtil {

	public static Map<String, JavaMapJdbc> jdbcTypeMap = new HashMap<String, JavaMapJdbc>();
	
	public static Map<String, JavaMapJdbc> javaTypeMap = new HashMap<String, JavaMapJdbc>();
	
	static {
		JavaMapJdbc bit = new JavaMapJdbc("BIT", Boolean.class);
		JavaMapJdbc tinyint = new JavaMapJdbc("TINYINT", Byte.class);
		JavaMapJdbc Int = new JavaMapJdbc("INT", Integer.class);
		JavaMapJdbc smallint = new JavaMapJdbc("SMALLINT", Short.class);
		JavaMapJdbc integer= new JavaMapJdbc("INTEGER", Integer.class);
		JavaMapJdbc bigint = new JavaMapJdbc("BIGINT", Long.class);

		JavaMapJdbc Float = new JavaMapJdbc("FLOAT", java.lang.Float.class);
		JavaMapJdbc Double = new JavaMapJdbc("DOUBLE", java.lang.Double.class);
		JavaMapJdbc decimal = new JavaMapJdbc("DECIMAL", java.math.BigDecimal.class);
		JavaMapJdbc Char = new JavaMapJdbc("CHAR", String.class);
		JavaMapJdbc varchar = new JavaMapJdbc("VARCHAR", String.class);
		JavaMapJdbc longvarchar = new JavaMapJdbc("LONGVARCHAR", String.class);
		JavaMapJdbc clob = new JavaMapJdbc("CLOB", String.class);

		JavaMapJdbc date = new JavaMapJdbc("DATE", java.util.Date.class);
		JavaMapJdbc time = new JavaMapJdbc("TIME", java.util.Date.class);
		JavaMapJdbc timestamp = new JavaMapJdbc("TIMESTAMP", java.util.Date.class);
		JavaMapJdbc datetime = new JavaMapJdbc("DATETIME", java.util.Date.class);
		JavaMapJdbc binary = new JavaMapJdbc("BINARY", Byte[].class);
		JavaMapJdbc varbinary = new JavaMapJdbc("VARBINARY", Byte[].class);
		JavaMapJdbc longbinary = new JavaMapJdbc("LONGVARBINARY", Byte[].class);
		JavaMapJdbc blob = new JavaMapJdbc("BLOB", Byte[].class);
		
		jdbcTypeMap.put("BIT", bit);
		jdbcTypeMap.put("TINYINT", tinyint);
		jdbcTypeMap.put("SMALLINT", smallint);
		jdbcTypeMap.put("INTEGER", integer);
		jdbcTypeMap.put("INT", Int);
		jdbcTypeMap.put("BIGINT", bigint);
		
		jdbcTypeMap.put("FLOAT", Float);
		jdbcTypeMap.put("DOUBLE", Double);
		jdbcTypeMap.put("DECIMAL", decimal);
		jdbcTypeMap.put("CHAR", Char);
		jdbcTypeMap.put("VARCHAR", varchar);
		jdbcTypeMap.put("LONGVARCHAR", longvarchar);
		jdbcTypeMap.put("CLOB", clob);
		
		jdbcTypeMap.put("DATE", date);
		jdbcTypeMap.put("TIME", time);
		jdbcTypeMap.put("TIMESTAMP", timestamp);
		jdbcTypeMap.put("DATETIME", datetime);
		jdbcTypeMap.put("BINARY", binary);
		jdbcTypeMap.put("VARBINARY", varbinary);
		jdbcTypeMap.put("LONGVARBINARY", longbinary);
		jdbcTypeMap.put("BLOB", blob);
		jdbcTypeMap.put("REAL", Float);
	}
	
	public static JavaMapJdbc getJavaMapJdbc(String jdbcType) {
		return jdbcTypeMap.get(jdbcType);
	}
}
