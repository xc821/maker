package ldh.maker.util;

import javafx.scene.Node;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import ldh.maker.component.ContentUi;
import ldh.maker.component.TableUi;
import ldh.maker.component.ContentUiFactory;
import ldh.maker.vo.TreeNode;
import org.controlsfx.control.StatusBar;
//import org.h2.tools.Server;

import java.sql.Connection;

/**
 * Created by ldh on 2017/2/26.
 */
public class UiUtil {

    public static Stage STAGE;
    public static TreeView<TreeNode> TREE;
//    public static Server H2SQL;
    public static Connection H2CONN;

    public volatile static ContentUiFactory FACTORY;

    public volatile static StatusBar STATUSBAR;

    private volatile static String TYPE = null;

    public static void setType(String type) {
        TYPE = type;
    }

    public static String getTYPE() {
        return TYPE;
    }

    public static TreeNode getCurrentTreeNode() {
        return TREE.getSelectionModel().getSelectedItem().getValue();
    }

    public static TreeItem<TreeNode> getCurrentTreeItem() {
        return TREE.getSelectionModel().getSelectedItem();
    }

    public static void setContentUiFactory(ContentUiFactory contentUiFactory) {
        FACTORY = contentUiFactory;
    }

    public static synchronized ContentUi getContentUi() {
        if (FACTORY == null) {
            FACTORY = new ContentUiFactory();
        }
        return  FACTORY.create();
    }

    public static void hideChildren(Pane pane) {
        for(Node child : pane.getChildren()) {
            child.setVisible(false);
        }
    }

}
