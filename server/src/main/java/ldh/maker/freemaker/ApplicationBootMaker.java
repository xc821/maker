package ldh.maker.freemaker;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by ldh on 2017/4/8.
 */
public class ApplicationBootMaker extends FreeMarkerMaker<ApplicationBootMaker> {

    protected String projectRootPackage;
    protected String author;

    public ApplicationBootMaker projectRootPackage(String projectRootPackage) {
        this.projectRootPackage = projectRootPackage;
        return this;
    }
    public ApplicationBootMaker author(String author) {
        this.author = author;
        return this;
    }

    @Override
    public ApplicationBootMaker make() {
        data();
        if (ftl == null) {
            this.out("applicationBoot.ftl", data);
        } else {
            this.out(ftl, data);
        }
        return this;
    }

    @Override
    public void data() {
        SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String str=sdf.format(new Date());
        if (fileName == null) {
            fileName = "ApplicationBoot.java";
        }
        data.put("Author",author);
        data.put("DATE", str);
        data.put("projectRootPackage", projectRootPackage);
    }
}
