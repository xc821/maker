spring:
  profiles:
    active: dev

  application:
    name: ${projectName}

  datasource:
    name: hikariDataSource
    type: com.zaxxer.hikari.HikariDataSource
    driver-class-name: com.mysql.cj.jdbc.Driver
    url: jdbc:h2:mem:test;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=FALSE
    username: sa
    password:
    hikari:
      minimum-idle: 1
      maximum-pool-size: 3
      auto-commit: true
      idle-timeout: 30000
      pool-name: DatebookHikariCP
      max-lifetime: 1800000
      connection-timeout: 30000
      connection-test-query: SELECT 1

  mvc:
    view:
      prefix: .jsp
      suffix: .jsp

server:
  port: 8282